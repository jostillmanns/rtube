$(document).ready(function(){    
    var sel_channel = document.getElementById("channel_sel");
    var sel_video = document.getElementById("video_sel");
    var but_unview = document.getElementById ("unview");
    var but_update = document.getElementById("update");
    var but_view = document.getElementById("view");
    var vid_info = document.getElementById("vid_info");
    var div_load = document.getElementById("load_gif");
    var first_video_index = 1;

    function update_video(v_id) {
	$.post("video_sel",{id:v_id},function(data){
	    first_video_index = data[0].id;
	    $.each(data,function(){
		op = new Option(this.title ,this.id);
		if(this.viewed)op.style.color = 'red';
		sel_video.options[sel_video.options.length] = op;
	    });
	},"json");
    }

    function update_vid_info(v_id) {
	$.post("vid_info",{id:v_id},function(data){
	    vid_info.innerHTML = data;
	});
    }

     function load(){
	sel_video.deactivated = true;
	sel_channel.deactivated = true;
	div_load.innerHTML = "<img src='load.gif' />";
    }

    function unload(){
	sel_video.deactivated = false;
	sel_channel.deactivated = false;
	div_load.innerHTML = "";
    }

    function update_channel() {
	sel_channel.options.length = 0;
	$.post("channel_sel", function(data){
	    $.each(data, function(){	    
		sel_channel.options[sel_channel.options.length] = new Option(this.name + " (" + this.unviewed + ")",this.id);
	    });
	},"json");
    }

    //init channel list
    update_channel();
       
    //init video list
    update_video(1);
    update_vid_info(first_video_index);

    //channel selection
    sel_channel.onchange = function(){
	sel_video.options.length = 0;
	update_video(sel_channel.options[sel_channel.selectedIndex].value);
    };

    //video selection
    sel_video.ondblclick = function(){
	$.post("video_play",{id:sel_video.options[this.selectedIndex].value});
	sel_video.options[this.selectedIndex].style.color="red";
    };

    but_unview.onclick = function(){
    	$.post("unview",{id:sel_video.options[sel_video.selectedIndex].value});
    	sel_video.options[sel_video.selectedIndex].style.color="black";
    };
    but_update.onclick = function(){
	load();
    	$.post("update",function(data){
	    unload();
	    update_channel();
	    update_video(1);
	});
    };
    but_view.onclick = function(){
	load();
    	$.post("view",{id:sel_video.options[sel_video.selectedIndex].value},function(data) {
	    if(data == "true"){
		sel_video.options[sel_video.selectedIndex].style.color="red";
		if(sel_video.selectedIndex < sel_video.options.length)
		    sel_video.options[sel_video.options.selectedIndex+1].selected = true;
	    }
	    else
		alert("fail")
	});
	unload();
    };
    
    sel_video.onclick = function(){
	update_vid_info(sel_video.options[sel_video.selectedIndex].value);
    };
});
