# model.rb

require 'rubygems'
require 'data_mapper'

DataMapper::Logger.new("log/server.log", :debug)

DataMapper.setup(:default, 'sqlite:rtuby.sqlite3')

class Video
  include DataMapper::Resource

  property :id, Serial
  property :title, String
  property :url, String
  property :date, DateTime
  property :viewed, Boolean, :default => false
  belongs_to :channel
end

class Channel
  include DataMapper::Resource

  property :id, Serial
  property :name, String
  property :url, String
  has n, :videos

  def unviewed
    Video.all(:channel => self, :viewed => false).count
  end
end  

DataMapper.finalize
