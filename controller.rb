# controller.rb
require 'json'
require 'data_mapper'
require 'dm-serializer'
require 'rss'
require 'open-uri'
require 'sinatra'
require_relative 'model'

CHANNELS = ['GameGrumps',
            'sempervideo',
            'continueshow',
            'darkvikt0ry',
            'coldmirror',
            'jontronshow',
            'TheAmazingAtheist',
            'DieBananenrepublik',
            'andrewmfilms',
            'thenewbostonTV',
            'DYKGaming',
            'egoraptor',
            'jaxamoto',
            'OneyNG',
            'SemperCensio',
            'TotallySketch',
            'psychicpebbles']

class Rtube < Sinatra::Base
  
  #Action Methods
  get '/' do
    erb :index
  end

  post '/channel_sel' do
    post_channel 
  end

  post '/video_sel' do
    post_video params[:id]
  end

  post '/video_play' do
    video = Video.first(:id => params[:id])
    video.update(:viewed => true)
    video.save!
    play video.url.to_s    
  end

  post '/unview' do
    unview params[:id]
  end

  post '/update' do
    puts "updating ..."
    db_update
  end

  post '/view' do
    view params[:id]
  end

  post '/vid_info' do
    post_single_video params[:id]
  end
  
  #Helper Methods
  def db_update
    DataMapper.auto_upgrade!
    CHANNELS.each do |chan|
      u = 'http://www.youtube.com/rss/user/' + chan + '/videos.rss'
      open(u) do |rss|
        feed = RSS::Parser.parse(rss)
        channel = Channel.first_or_create(
                                          :name => feed.channel.title.gsub("Uploads by", ""),
                                          :url => u
                                          )
        channel.save!
        feed.items.each do |item|
          video = Video.first_or_create(
                                        :title => item.title,
                                        :url => item.link.split("&")[0],
                                        :date => item.pubDate,
                                        :channel => channel
                                        )
          video.save!
        end
      end
    end
    "true"
  end

  def post_channel
    Channel.all.to_json(:only => [:id,:name], :methods => [:unviewed])
  end

  def post_video id
    chan = Channel.first(:id => id)
    Video.all(:channel => chan,:order => [:date.desc]).to_json(:only => [:id,:title,:viewed])
  end

  def post_single_video id
    v = Video.first(:id => id)
    "<b>#{v.title}</b> [#{v.date.strftime('%d.%m.%y')}] <a href='#{v.url}'>#{v.url}</a>"
  end

  def play url
    system("youtube-viewer -I --really-quiet #{url}")
  end

  def unview id
    vid = Video.first(:id => id)
    vid.update(:viewed => false)
    vid.save!
  end

  def view id
    vid = Video.first(:id => id)
    vid.update(:viewed => true)
    vid.save!
    "true"
  end
end


